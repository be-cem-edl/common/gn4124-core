..
  SPDX-License-Identifier: CC0-1.0

  SPDX-FileCopyrightText: 2019-2020 CERN

==========
Change Log
==========
- Format inspired by: `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`_
- Versioning scheme follows: `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`_

3.1.2 - 2023-11-20
==================
https://www.ohwr.org/project/gn4124-core/tree/v3.1.2

Changed
-------
- hdl: pipeline l2p arbiter outputs to relax timing (#3)
- hdl: simplify l2p_dma_master FIFO reset logic (#4)

3.1.1 - 2022-11-03
==================
https://www.ohwr.org/project/gn4124-core/tree/v3.1.1

Fixed
-----
- hdl: adjust component

3.1.0 - 2020-11-09
==================
https://www.ohwr.org/project/gn4124-core/tree/v3.1.0

Fixed
-----
- hdl: L2P DMA cross page issue
- hdl: L2P incorrect handling of stall signal
- hdl: P2L allow transfer longer than 4KB

3.0.1 - 2020-09-29
==================
https://www.ohwr.org/project/gn4124-core/tree/v3.0.1

Fixed
-----
- hdl: L2P DMA issues reported with slower hosts

3.0.0 - 2020-07-27
==================
https://www.ohwr.org/project/gn4124-core/tree/v3.0.0

Added
-----
- hdl: SystemVerilog BFM and testbench.
- hdl: Add wrapper with wishbone records and slave adapters.
- hdl: Add generics to tune the depths of the various async FIFOs.

Changed
-------
- hdl: Major rewrite of DMA engine, in particular the L2P DMA Master.
- hdl: Major cleanup of resets and cross-clock domain synchronisation.
- hdl: Stop using coregen FIFOs, switch to FIFOs from general-cores.
- hdl: Make DMA optional (g_WITH_DMA generic).
- hdl: Use cheby to describe registers, only one interrupt (level).
- hdl: Test, verify and enable byte swap feature.
- hdl: Extend SV BFM with tasks to read/write from simulated host memory.

Fixed
-----
- hdl: Fixed incorrect 64-bit DMA transaction generation bug.
- hdl: Allow larger DMA reads (up to the full 32 bits of the "length" register) for L2P DMA master.
- hdl: Add flow control to the write buffer of the BFM to prevent overflows during 'wr' commands.
- hdl: Fix swapped bits in attributes.
- hdl: Handle host 32-bit address overflow in L2P DMA master.
- hdl: Fix bug in BFM not respecting P2L_RDY during DMA writes.
- hdl: Fix bug in BFM not accepting 4096B writes.

2.0.0 - 2014-04-03
==================
https://www.ohwr.org/project/gn4124-core/tree/v2.0.0

Added
-----
- Second release of gn4124-core

1.0.0 - 2013-03-01
==================
https://www.ohwr.org/project/gn4124-core/tree/v1.0.0

Added
-----
- First release of gn4124-core
