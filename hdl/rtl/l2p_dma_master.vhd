--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- GN4124 core for PCIe FMC carrier
-- http://www.ohwr.org/projects/gn4124-core
--------------------------------------------------------------------------------
--
-- unit name:   l2p_dma_master
--
-- description: 32 bit L2P DMA master. Provides a pipelined wishbone interface
-- that performs DMA transfer from the local application to PCI express host.
--
--------------------------------------------------------------------------------
-- Copyright CERN 2010-2020
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.gn4124_core_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.genram_pkg.all;

entity l2p_dma_master is
  generic (
    -- It is important to set this threshold such that the FIFO has room to store
    -- all pending read requests from the pipelined wishbone interface in case
    -- the Gennum decides to stall and the FIFO starts filling up. The default
    -- value is correct if the WB slave is the Spartan-6 DDR controller.
    g_FIFO_FULL_THRES : positive := 64;
    g_FIFO_SIZE       : positive := 256;
    g_BYTE_SWAP       : boolean  := FALSE);
  port (
    -- GN4124 core clk and reset
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    -- From the DMA controller
    dma_ctrl_target_addr_i : in  std_logic_vector(31 downto 0);
    dma_ctrl_host_addr_h_i : in  std_logic_vector(31 downto 0);
    dma_ctrl_host_addr_l_i : in  std_logic_vector(31 downto 0);
    dma_ctrl_len_i         : in  std_logic_vector(31 downto 0);
    dma_ctrl_start_l2p_i   : in  std_logic;
    dma_ctrl_done_o        : out std_logic;
    dma_ctrl_error_o       : out std_logic;
    dma_ctrl_byte_swap_i   : in  std_logic_vector(1 downto 0);
    dma_ctrl_abort_i       : in  std_logic;

    -- To the arbiter (L2P data)
    ldm_arb_valid_o  : out std_logic;
    ldm_arb_dframe_o : out std_logic;
    ldm_arb_data_o   : out std_logic_vector(31 downto 0);
    ldm_arb_req_o    : out std_logic;
    ldm_arb_gnt_i    : in  std_logic;


    -- L2P channel control
    l2p_edb_o  : out std_logic;  -- Asserted when transfer is aborted
    l_wr_rdy_i : in  std_logic;  -- Asserted when GN4124 is ready to receive master write
    l2p_rdy_i  : in  std_logic;  -- De-asserted to pause transfer already in progress
    tx_error_i : in  std_logic;  -- Asserted when unexpected or malformed paket received

    -- DMA Interface (Pipelined Wishbone Master)
    wb_dma_rst_n_i : in  std_logic;  -- Active low reset in sync with wb_dma_clk_i
    wb_dma_clk_i   : in  std_logic;
    wb_dma_i       : in  t_wishbone_master_in;
    wb_dma_o       : out t_wishbone_master_out);
end l2p_dma_master;

architecture arch of l2p_dma_master is

  -- Used to tweak the almost full flag threshold of the SYNC FIFO
  -- in order to help with timing by giving an advanced warning
  -- that we can then pipeline through an equal number of registers.
  constant c_SYNC_FIFO_FULL_DELAY : natural := 3;

  -- Even though the actual limit is 4KiB, the GN4124 really hates it
  -- if we try to send more than 256 Bytes (64 Words) within a single packet.
  -- During tests, we've seen that the GN4124 chip might freeze when such
  -- a request arrives, with the probability of a freeze increasing with
  -- the size of the packet.
  -- The overhead of the extra transaction is minimal so we keep here the
  -- limit that was there in previous versions of this code (128 Bytes,
  -- or 32 Words).
  constant c_L2P_MAX_PAYLOAD : integer := 32;

  type l2p_dma_state_type is (L2P_IDLE, L2P_WB_SETUP, L2P_SETUP,
                              L2P_WAIT, L2P_HEADER, L2P_HOLD,
                              L2P_ADDR_H, L2P_ADDR_L, L2P_DATA,
                              L2P_ERROR);
  signal l2p_dma_current_state : l2p_dma_state_type := L2P_IDLE;

  type wb_dma_state_type is (WB_IDLE, WB_SETUP, WB_DATA, WB_WAIT_ACK);
  signal wb_dma_current_state : wb_dma_state_type := WB_IDLE;

  signal dma_target_addr : unsigned(31 downto 2)        := (others => '0');
  signal dma_total_len   : unsigned(31 downto 2)        := (others => '0');
  signal dma_packet_len  : unsigned(5 downto 0)         := (others => '0'); --  In word
  signal dma_host_addr   : unsigned(63 downto 0)        := (others => '0');
  signal dma_byte_swap   : std_logic_vector(1 downto 0) := (others => '0');

  alias dma_host_addr_h : unsigned(31 downto 0) is dma_host_addr(63 downto 32);
  alias dma_host_addr_l : unsigned(31 downto 0) is dma_host_addr(31 downto 0);

  signal l2p_64b_address : std_logic := '0';
  signal dma_size_page : unsigned(12 downto 2);

  signal l2p_fsm_valid  : std_logic                     := '0';
  signal l2p_fsm_dframe : std_logic                     := '0';
  signal l2p_fsm_data   : std_logic_vector(31 downto 0) := (others => '0');

  signal l2p_fsm_dma_param_wr   : std_logic                     := '0';
  signal l2p_fsm_dma_param_busy : std_logic                     := '0';
  signal dma_param_sync         : std_logic_vector(59 downto 0) := (others => '0');
  signal dma_param_to_sync      : std_logic_vector(59 downto 0);
  signal dma_param_wr           : std_logic                     := '0';

  signal l2p_timeout_cnt : unsigned(12 downto 0) := (others => '0');

  signal wb_dma_cyc         : std_logic := '0';
  signal wb_dma_stb         : std_logic := '0';
  signal wb_dma_fsm_en      : std_logic := '0';
  signal wb_dma_fsm_en_sync : std_logic := '0';

  signal data_fifo_rd     : std_logic                     := '0';
  signal data_fifo_wr     : std_logic                     := '0';
  signal data_fifo_empty  : std_logic                     := '1';
  signal data_fifo_full   : std_logic                     := '0';
  signal data_fifo_din    : std_logic_vector(31 downto 0) := (others => '0');
  signal data_fifo_dout   : std_logic_vector(31 downto 0) := (others => '0');
  signal data_fifo_dout_d : std_logic_vector(31 downto 0) := (others => '0');

  signal fsm_fifo_rst_n : std_logic := '0';

  signal data_fifo_full_d    : std_logic_vector(c_SYNC_FIFO_FULL_DELAY - 1 downto 0) := (others => '0');
  signal data_fifo_full_next : std_logic;
  signal data_fifo_rst_wr_n  : std_logic;
  signal data_fifo_rst_rd_n  : std_logic;

begin

  -- 64bit address flag used to generate the L2P header and as an input to
  -- the L2P FSM to control the transition through the L2P_ADDR states.
  l2p_64b_address <= '0' when dma_host_addr_h = x"00000000" else '1';

  ---------------------------------------
  -- L2P FSM (in the Gennum clock domain)
  ---------------------------------------
  p_l2p_fsm : process (clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        l2p_dma_current_state <= L2P_IDLE;
        ldm_arb_req_o         <= '0';
        l2p_fsm_valid         <= '0';
        l2p_fsm_dframe        <= '0';
        dma_ctrl_done_o       <= '0';
        dma_ctrl_error_o      <= '0';
        wb_dma_fsm_en         <= '0';
        l2p_edb_o             <= '0';
        fsm_fifo_rst_n        <= '0';
        data_fifo_rd          <= '0';
        l2p_fsm_dma_param_wr  <= '0';
      else

        data_fifo_dout_d <= data_fifo_dout;

        -- default values if not overriden by current state
        ldm_arb_req_o        <= '0';
        l2p_fsm_valid        <= '0';
        l2p_fsm_dframe       <= '0';
        l2p_fsm_dma_param_wr <= l2p_fsm_dma_param_busy;
        dma_ctrl_done_o      <= '0';
        dma_ctrl_error_o     <= '0';
        l2p_edb_o            <= '0';
        fsm_fifo_rst_n       <= '1';
        data_fifo_rd         <= '0';
        wb_dma_fsm_en        <= '1';
        l2p_timeout_cnt      <= (others => '0');

        case l2p_dma_current_state is

          when L2P_IDLE =>
            wb_dma_fsm_en   <= '0';
            fsm_fifo_rst_n  <= '0';
            if dma_ctrl_start_l2p_i = '1' then
              dma_target_addr       <= unsigned(dma_ctrl_target_addr_i(31 downto 2));
              dma_host_addr_h       <= unsigned(dma_ctrl_host_addr_h_i);
              dma_host_addr_l       <= unsigned(dma_ctrl_host_addr_l_i);
              dma_total_len         <= unsigned(dma_ctrl_len_i(31 downto 2));
              dma_byte_swap         <= dma_ctrl_byte_swap_i;
              l2p_dma_current_state <= L2P_WB_SETUP;
            end if;

          when L2P_WB_SETUP =>
            --  Start to fill the data fifo (from DDR).  Need to cross clock domain.
            l2p_fsm_dma_param_wr <= '1';
            if l2p_fsm_dma_param_busy = '1' then
              l2p_dma_current_state <= L2P_SETUP;
            end if;

          when L2P_SETUP =>
            -- Calculate DMA packet length for next tranfer. A transfer can be
            -- up to 1024 words, limited by the "length" field in the L2P header.
            -- We limit it artificially to c_L2P_MAX_PAYLOAD (see note in
            -- constant declaration).
            if dma_total_len > c_L2P_MAX_PAYLOAD then
              dma_packet_len  <= to_unsigned(c_L2P_MAX_PAYLOAD, dma_packet_len'length);
            else
              dma_packet_len  <= dma_total_len(7 downto 2);
            end if;
            --  Compute the number of word remaining in the current host page.
            dma_size_page <= ('0' & not dma_host_addr(11 downto 2)) + 1;
            l2p_dma_current_state <= L2P_WAIT;

          when L2P_WAIT =>
            -- A PCI transfer cannot cross a 4-KB boundary
            -- (See PCI Express Base Specification Revision 5.0 version 1.0 p 128)
            if dma_packet_len > dma_size_page then
              dma_packet_len <= dma_size_page(7 downto 2);
            end if;
            -- Send request to DMA arbiter
            ldm_arb_req_o <= not ldm_arb_gnt_i;
            -- Move to next state when:
            -- a) granted DMA access by arbiter
            -- b) there is data waiting in the FIFO
            -- c) Gennum is ready
            if ldm_arb_gnt_i = '1' and data_fifo_empty = '0' and
              l_wr_rdy_i = '1' and l2p_rdy_i = '1'
            then
              l2p_dma_current_state <= L2P_HEADER;
            end if;

          -- Note: we don't check l2p_rdy_i again until we reach the L2P_DATA state.
          -- That's ok, according to Gennum, we have up to 7 clock cycles to
          -- drop ldm_arb_valid after the Gennum drops l2p_rdy.
          when L2P_HEADER =>
            -- Must keep dframe asserted to stay bus master until end of transfer
            l2p_fsm_dframe   <= '1';
            l2p_fsm_valid    <= '1';
            l2p_fsm_data     <= (others => '0');
            -- Header type
            l2p_fsm_data(25) <= '1';
            l2p_fsm_data(24) <= l2p_64b_address;
            -- LBE (Last Byte Enable) must be "0000" only
            -- when the length field is equal to 1
            if dma_packet_len /= 1 then
              l2p_fsm_data(23 downto 20) <= "1111";
            end if;
            -- FBE (First Byte Enable)
            l2p_fsm_data(19 downto 16) <= "1111";
            -- Length field (in 32 bit words). When zero it means 1024 words.
            l2p_fsm_data(9 downto 0)   <= "0000" & std_logic_vector(dma_packet_len);
            if l2p_64b_address = '1' then
              l2p_dma_current_state <= L2P_ADDR_H;
            else
              l2p_dma_current_state <= L2P_ADDR_L;
            end if;

          when L2P_ADDR_H =>
            l2p_fsm_dframe        <= '1';
            l2p_fsm_valid         <= '1';
            l2p_fsm_data          <= std_logic_vector(dma_host_addr_h);
            l2p_dma_current_state <= L2P_ADDR_L;

          when L2P_ADDR_L =>
            l2p_fsm_dframe        <= '1';
            l2p_fsm_valid         <= '1';
            l2p_fsm_data          <= std_logic_vector(dma_host_addr_l);
            -- Already checked data_fifo_empty flag during L2P_WAIT.
            -- Start readout here to get first data out on the next cycle.
            data_fifo_rd          <= '1';
            --  Update host address (for the next transfer)
            dma_host_addr         <= dma_host_addr + 4*dma_packet_len;
            dma_total_len         <= dma_total_len - dma_packet_len;
            l2p_dma_current_state <= L2P_DATA;

          when L2P_DATA =>
            l2p_fsm_dframe <= '1';
            -- Data FIFO readout
            if data_fifo_empty = '1' or l2p_rdy_i = '0' then
              --  Not ready - wait
              l2p_dma_current_state <= L2P_HOLD;
            else
              data_fifo_rd   <= '1';
              l2p_fsm_valid  <= '1';
              l2p_fsm_data   <= f_byte_swap(g_BYTE_SWAP, data_fifo_dout, dma_byte_swap);
              dma_packet_len <= dma_packet_len - 1;
              -- Detect end of transfer
              if dma_packet_len = 1 then
                l2p_fsm_dframe <= '0';
                data_fifo_rd   <= '0';
                if dma_total_len /= 0 then
                  --  There a still data to transfer, continue.
                  l2p_dma_current_state <= L2P_SETUP;
                else
                  --  No more data, done.
                  l2p_dma_current_state <= L2P_IDLE;
                  dma_ctrl_done_o       <= '1';
                end if;
              end if;
            end if;
            -- Timeout counter, it is reset to 0 by default FSM value
            -- if not increased here.
            if l2p_rdy_i = '1' then
              l2p_timeout_cnt <= l2p_timeout_cnt + 1;
            end if;
            -- Check for errors
            if tx_error_i = '1' or dma_ctrl_abort_i = '1' or l2p_timeout_cnt = x"fff" then
              l2p_fsm_dframe        <= '0';
              l2p_dma_current_state <= L2P_ERROR;
            end if;

          when L2P_HOLD =>
            l2p_fsm_dframe <= '1';
            l2p_fsm_valid  <= '0';
            if data_fifo_empty = '0' and l2p_rdy_i = '1' then
              data_fifo_rd          <= '1';
              l2p_dma_current_state <= L2P_DATA;
            end if;

          when L2P_ERROR =>
            wb_dma_fsm_en         <= '0';
            fsm_fifo_rst_n        <= '0';
            dma_ctrl_error_o      <= '1';
            l2p_edb_o             <= '1';
            l2p_dma_current_state <= L2P_IDLE;

          when others =>
            l2p_dma_current_state <= L2P_ERROR;

        end case;
      end if;
    end if;
  end process p_l2p_fsm;

  ldm_arb_valid_o  <= l2p_fsm_valid;
  ldm_arb_dframe_o <= l2p_fsm_dframe;
  ldm_arb_data_o   <= l2p_fsm_data;

-------------------------------------------------
-- Wishbone Master (in the Wishbone clock domain)
-------------------------------------------------

  --  Info crossing clocks domains (GN -> DDR) at the start of a dma transfer.
  dma_param_to_sync(59 downto 30) <= std_logic_vector(dma_target_addr);
  dma_param_to_sync(29 downto 0)  <= std_logic_vector(dma_total_len);

  cmp_sync_dma_param : entity work.gc_sync_word_wr
    generic map (
      g_AUTO_WR => FALSE,
      g_WIDTH   => 60)
    port map (
      clk_in_i    => clk_i,
      rst_in_n_i  => '1',
      clk_out_i   => wb_dma_clk_i,
      rst_out_n_i => '1',
      data_i      => dma_param_to_sync,
      wr_i        => l2p_fsm_dma_param_wr,
      busy_o      => l2p_fsm_dma_param_busy,
      ack_o       => open,
      data_o      => dma_param_sync,
      wr_o        => dma_param_wr);

  cmp_wb_dma_fsm_en_sync : gc_sync
    port map (
      clk_i     => wb_dma_clk_i,
      rst_n_a_i => '1',
      d_i       => wb_dma_fsm_en,
      q_o       => wb_dma_fsm_en_sync);

  wb_dma_o.cyc <= wb_dma_cyc;
  wb_dma_o.stb <= wb_dma_stb;
  wb_dma_o.we  <= '0';
  wb_dma_o.sel <= (others => '1');
  wb_dma_o.dat <= (others => '0');

  -- No need to check FIFO full, it was done earlier
  -- when we decided to strobe (in combination with a proper
  -- almost_full flag from the FIFO).
  data_fifo_din <= wb_dma_i.dat;
  data_fifo_wr  <= wb_dma_i.ack;

  p_wb_fsm : process (wb_dma_clk_i)
    variable wb_dma_addr    : unsigned(29 downto 0) := (others => '0');
    variable wb_dma_cnt_stb : unsigned(29 downto 0) := (others => '0');
    variable wb_dma_cnt_ack : unsigned(29 downto 0) := (others => '0');
  begin
    if rising_edge(wb_dma_clk_i) then
      if wb_dma_rst_n_i = '0' or wb_dma_fsm_en_sync = '0' then
        wb_dma_cyc           <= '0';
        wb_dma_stb           <= '0';
        wb_dma_addr          := (others => '0');
        wb_dma_o.adr         <= (others => '0');
        wb_dma_current_state <= WB_IDLE;
      else

        case wb_dma_current_state is

          when WB_IDLE =>
            -- Start when the new DMA parameters (address, size) have been received
            if dma_param_wr = '1' then
              wb_dma_addr          := unsigned(dma_param_sync(59 downto 30));
              wb_dma_cnt_stb       := unsigned(dma_param_sync(29 downto 0));
              wb_dma_cnt_ack       := unsigned(dma_param_sync(29 downto 0));
              wb_dma_current_state <= WB_SETUP;
            end if;

          when WB_SETUP =>
            -- Start/maintain the WB cycle
            wb_dma_cyc <= '1';
            -- Always keep track of pending ACKs
            if wb_dma_i.ack = '1' then
              wb_dma_cnt_ack := wb_dma_cnt_ack - 1;
            end if;
            -- If there is space in the FIFO, send the first/next read request
            if data_fifo_full = '0' then
              wb_dma_o.adr         <= "00" & std_logic_vector(wb_dma_addr);
              wb_dma_stb           <= '1';
              wb_dma_current_state <= WB_DATA;
            end if;

          when WB_DATA =>
            -- Maintain the WB cycle
            wb_dma_cyc <= '1';
            -- Always keep track of pending ACKs
            if wb_dma_i.ack = '1' then
              wb_dma_cnt_ack := wb_dma_cnt_ack - 1;
            end if;
            -- If the slave was not stalling on the previous cycle,
            -- update address and counters
            if wb_dma_i.stall = '0' then
              wb_dma_addr    := wb_dma_addr + 1;
              wb_dma_cnt_stb := wb_dma_cnt_stb - 1;
              -- If all read requests have been issued, move to next state
              if wb_dma_cnt_stb = 0 then
                wb_dma_stb           <= '0';
                wb_dma_current_state <= WB_WAIT_ACK;
              -- If there is no room in the FIFO, drop strobe and wait
              elsif data_fifo_full = '1' then
                wb_dma_stb           <= '0';
                wb_dma_current_state <= WB_SETUP;
              -- Otherwise send the next request
              else
                wb_dma_o.adr <= "00" & std_logic_vector(wb_dma_addr);
                wb_dma_stb   <= '1';
              end if;
            end if;

          when WB_WAIT_ACK =>
            -- Maintain the WB cycle
            wb_dma_cyc <= '1';
            -- Always keep track of pending ACKs
            if wb_dma_i.ack = '1' then
              wb_dma_cnt_ack := wb_dma_cnt_ack - 1;
            end if;
            -- If all ACKs have been received, we are done
            if wb_dma_cnt_ack = 0 then
              wb_dma_cyc           <= '0';
              wb_dma_current_state <= WB_IDLE;
            end if;

          when others =>
            wb_dma_current_state <= WB_IDLE;

        end case;
      end if;
    end if;
  end process p_wb_fsm;

  -----------------------------------------
  -- Flow Control FIFO (cross-clock domain)
  -----------------------------------------

  cmp_data_fifo_rst_rd_n_sync : gc_sync
    port map (
      clk_i     => clk_i,
      rst_n_a_i => fsm_fifo_rst_n,
      d_i       => wb_dma_rst_n_i,
      q_o       => data_fifo_rst_rd_n);

  cmp_data_fifo_rst_wr_n_sync : gc_sync
    port map (
      clk_i     => wb_dma_clk_i,
      rst_n_a_i => wb_dma_rst_n_i,
      d_i       => fsm_fifo_rst_n,
      q_o       => data_fifo_rst_wr_n);

  p_fifo_full_delay_reg : process (wb_dma_clk_i) is
  begin
    if rising_edge(wb_dma_clk_i) then
      -- we want proper registers to help with timing and
      -- having a reset prevents inferring of shift register.
      if data_fifo_rst_wr_n = '0' then
        data_fifo_full_d <= (others => '0');
      else
        data_fifo_full_d <= data_fifo_full_d(data_fifo_full_d'high-1 downto 0) & data_fifo_full_next;
      end if;
    end if;
  end process p_fifo_full_delay_reg;

  data_fifo_full <= data_fifo_full_d(data_fifo_full_d'high);

  cmp_data_fifo : generic_async_fifo_dual_rst
    generic map (
      g_DATA_WIDTH            => 32,
      g_SIZE                  => g_FIFO_SIZE,
      g_SHOW_AHEAD            => TRUE,
      g_WITH_WR_FULL          => FALSE,
      g_WITH_WR_ALMOST_FULL   => TRUE,
      g_ALMOST_FULL_THRESHOLD => g_FIFO_FULL_THRES - c_SYNC_FIFO_FULL_DELAY)
    port map (
      -- write port
      rst_wr_n_i       => data_fifo_rst_wr_n,
      clk_wr_i         => wb_dma_clk_i,
      d_i              => data_fifo_din,
      we_i             => data_fifo_wr,
      wr_almost_full_o => data_fifo_full_next,
      -- read port
      rst_rd_n_i       => data_fifo_rst_rd_n,
      clk_rd_i         => clk_i,
      q_o              => data_fifo_dout,
      rd_i             => data_fifo_rd,
      rd_empty_o       => data_fifo_empty);

end architecture arch;
