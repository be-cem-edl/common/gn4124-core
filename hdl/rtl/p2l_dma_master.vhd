--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- GN4124 core for PCIe FMC carrier
-- http://www.ohwr.org/projects/gn4124-core
--------------------------------------------------------------------------------
--
-- unit name:   p2l_dma_master
--
-- description: 32 bit P2L DMA master. Provides a pipelined Wishbone interface
-- that performs DMA transfers from PCI express host to local application.
-- This entity is also used to catch the next item in chained DMA.
--
-- IMPORTANT NOTE: This module has a known bug, where the FSM will consider a
-- transfer complete and signal the "done" to the DMA controller module without
-- waiting for the WB side to write all data. This can be a problem, especially
-- when the WB side is slower and the next DMA transaction is a read which will
-- switch the WB signals to the L2P module, thus cutting the previous WB write
-- transfer from this module in the middle.
--
--------------------------------------------------------------------------------
-- Copyright CERN 2010-2020
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

use work.gn4124_core_pkg.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.genram_pkg.all;

entity p2l_dma_master is
  generic (
    g_FIFO_SIZE : positive := 64;
    g_BYTE_SWAP : boolean  := FALSE);
  port (
      ---------------------------------------------------------
      -- GN4124 core clock and reset
      clk_i   : in std_logic;
      rst_n_i : in std_logic;

      ---------------------------------------------------------
      -- From the DMA controller
      dma_ctrl_carrier_addr_i : in  std_logic_vector(31 downto 0);
      dma_ctrl_host_addr_h_i  : in  std_logic_vector(31 downto 0);
      dma_ctrl_host_addr_l_i  : in  std_logic_vector(31 downto 0);
      dma_ctrl_len_i          : in  std_logic_vector(31 downto 0);
      dma_ctrl_start_p2l_i    : in  std_logic;
      dma_ctrl_start_next_i   : in  std_logic;
      dma_ctrl_done_o         : out std_logic;
      dma_ctrl_error_o        : out std_logic;
      dma_ctrl_byte_swap_i    : in  std_logic_vector(1 downto 0);
      dma_ctrl_abort_i        : in  std_logic;

      ---------------------------------------------------------
      -- From P2L Decoder (receive the read completion)
      --
      -- Header
      pd_pdm_hdr_start_i   : in std_logic;  -- Header strobe
      pd_pdm_hdr_length_i  : in std_logic_vector(9 downto 0);  -- Packet length in 32-bit words multiples
      pd_pdm_hdr_cid_i     : in std_logic_vector(1 downto 0);  -- Completion ID
      pd_pdm_master_cpld_i : in std_logic;  -- Master read completion with data
      pd_pdm_master_cpln_i : in std_logic;  -- Master read completion without data
      --
      -- Data
      pd_pdm_data_valid_i  : in std_logic;  -- Indicates Data is valid
      pd_pdm_data_last_i   : in std_logic;  -- Indicates end of the packet
      pd_pdm_data_i        : in std_logic_vector(31 downto 0);  -- Data
      pd_pdm_be_i          : in std_logic_vector(3 downto 0);  -- Byte Enable for data

      ---------------------------------------------------------
      -- P2L control
      p2l_rdy_o  : out std_logic;  -- De-asserted to pause transfer already in progress
      rx_error_o : out std_logic;       -- Asserted when transfer is aborted

      ---------------------------------------------------------
      -- To the P2L Interface (send the DMA Master Read request)
      pdm_arb_valid_o  : out std_logic;  -- Read completion signals
      pdm_arb_dframe_o : out std_logic;  -- Toward the arbiter
      pdm_arb_data_o   : out std_logic_vector(31 downto 0);
      pdm_arb_req_o    : out std_logic;
      arb_pdm_gnt_i    : in  std_logic;

      ---------------------------------------------------------
      -- DMA Interface (Pipelined Wishbone Master)
      wb_dma_rst_n_i : in  std_logic;  -- Active low reset in sync with wb_dma_clk_i
      wb_dma_clk_i   : in  std_logic;  -- Bus clock
      wb_dma_i       : in  t_wishbone_master_in;
      wb_dma_o       : out t_wishbone_master_out;

      ---------------------------------------------------------
      -- To the DMA controller
      next_item_carrier_addr_o : out std_logic_vector(31 downto 0);
      next_item_host_addr_h_o  : out std_logic_vector(31 downto 0);
      next_item_host_addr_l_o  : out std_logic_vector(31 downto 0);
      next_item_len_o          : out std_logic_vector(31 downto 0);
      next_item_next_l_o       : out std_logic_vector(31 downto 0);
      next_item_next_h_o       : out std_logic_vector(31 downto 0);
      next_item_attrib_o       : out std_logic_vector(31 downto 0);
      next_item_valid_o        : out std_logic
      );
end p2l_dma_master;


architecture arch of p2l_dma_master is

  -----------------------------------------------------------------------------
  -- Constants declaration
  -----------------------------------------------------------------------------

  -- Used to tweak the almost full flag threshold of the SYNC FIFO
  -- in order to help with timing by giving an advanced warning
  -- that we can then pipeline through an equal number of registers.
  constant c_SYNC_FIFO_FULL_DELAY : natural := 3;

  -- c_MAX_READ_REQ_SIZE is the maximum size (in 32-bit words) of the payload of a packet.
  -- Allowed c_MAX_READ_REQ_SIZE values are: 32, 64, 128, 256, 512, 1024.
  -- This constant must be set according to the GN4124 and motherboard chipset capabilities.
  constant c_MAX_READ_REQ_SIZE     : unsigned(10 downto 0) := to_unsigned(1024, 11);

  -----------------------------------------------------------------------------
  -- Signals declaration
  -----------------------------------------------------------------------------

  -- control signals
  signal is_next_item     : std_logic;
  signal completion_error : std_logic;
  signal dma_busy_error   : std_logic;
  signal dma_ctrl_done_t  : std_logic := '0';
  signal rx_error_t       : std_logic := '0';
  signal rx_error_d       : std_logic := '0';

  -- L2P packet generator
  signal l2p_address_h   : std_logic_vector(31 downto 0) := (others => '0');
  signal l2p_address_l   : std_logic_vector(31 downto 0) := (others => '0');
  signal l2p_len_cnt     : unsigned(29 downto 0)         := (others => '0');
  signal l2p_len_header  : unsigned(10 downto 0);
  signal l2p_64b_address : std_logic;

  signal pdm_arb_data : std_logic_vector(31 downto 0) := (others => '0');

  -- Target address counter
  signal target_addr_cnt : unsigned(29 downto 0) := (others => '0');

  -- sync fifo
  signal fifo_rst_n    : std_logic;
  signal wb_fifo_rst_n : std_logic;

  signal to_wb_fifo_empty     : std_logic;
  signal to_wb_fifo_full      : std_logic;
  signal to_wb_fifo_rd        : std_logic;
  signal to_wb_fifo_wr        : std_logic;
  signal to_wb_fifo_wr_d      : std_logic;
  signal to_wb_fifo_din       : std_logic_vector(63 downto 0) := (others => '0');
  signal to_wb_fifo_din_d     : std_logic_vector(63 downto 0) := (others => '0');
  signal to_wb_fifo_dout      : std_logic_vector(63 downto 0);
  signal to_wb_fifo_byte_swap : std_logic_vector(1 downto 0) := (others => '0');

  -- wishbone
  signal wb_ack_cnt      : unsigned(15 downto 0);
  signal wb_dma_out_stb  : std_logic;
  signal wb_dma_tfr     : boolean;

  -- P2L DMA read request FSM
  type p2l_dma_state_type is (P2L_IDLE, P2L_SETUP, P2L_HEADER, P2L_ADDR_H, P2L_ADDR_L, P2L_WAIT_READ_COMPLETION);
  signal p2l_dma_current_state : p2l_dma_state_type;
  signal p2l_data_cnt          : unsigned(10 downto 0) := (others => '0');

  signal next_item_carrier_addr : std_logic_vector(31 downto 0) := (others => '0');
  signal next_item_host_addr_h  : std_logic_vector(31 downto 0) := (others => '0');
  signal next_item_host_addr_l  : std_logic_vector(31 downto 0) := (others => '0');
  signal next_item_len          : std_logic_vector(31 downto 0) := (others => '0');
  signal next_item_next_l       : std_logic_vector(31 downto 0) := (others => '0');
  signal next_item_next_h       : std_logic_vector(31 downto 0) := (others => '0');
  signal next_item_attrib       : std_logic_vector(31 downto 0) := (others => '0');

  signal to_wb_fifo_full_d    : std_logic_vector(c_SYNC_FIFO_FULL_DELAY - 1 downto 0) := (others => '0');
  signal to_wb_fifo_full_next : std_logic;

begin
  -- Errors to DMA controller
  dma_ctrl_error_o <= dma_busy_error or completion_error;

  -----------------------------------------------------------------------------
  -- PCIe read request FSM
  -----------------------------------------------------------------------------
  -- Stores information for read request packet
  -- Can be a P2L DMA transfer or catching the next item of a chained DMA
  p_read_req_fsm : process (clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        p2l_dma_current_state <= P2L_IDLE;
        pdm_arb_req_o         <= '0';
        pdm_arb_valid_o       <= '0';
        pdm_arb_dframe_o      <= '0';
        dma_ctrl_done_t       <= '0';
        next_item_valid_o     <= '0';
        completion_error      <= '0';
        rx_error_t            <= '0';
        l2p_64b_address       <= '0';
        is_next_item          <= '0';
      else
        case p2l_dma_current_state is

          when P2L_IDLE =>
            -- Clear status bits
            dma_ctrl_done_t   <= '0';
            next_item_valid_o <= '0';
            completion_error  <= '0';
            rx_error_t        <= '0';
            -- Start a read request when a P2L DMA is initated or when the DMA
            -- controller asks for the next DMA info (in a chained DMA).
            if dma_ctrl_start_p2l_i = '1' or dma_ctrl_start_next_i = '1' then
              -- Stores DMA info locally
              l2p_address_h <= dma_ctrl_host_addr_h_i;
              l2p_address_l <= dma_ctrl_host_addr_l_i;
              l2p_len_cnt   <= unsigned(dma_ctrl_len_i(31 downto 2));  -- dma_ctrl_len_i is in byte
              -- Catching whether this is a P2L DMA transfer or just a fetch of the next DMA item
              is_next_item <= dma_ctrl_start_next_i;
              if dma_ctrl_host_addr_h_i = X"00000000" then
                l2p_64b_address <= '0';
              else
                l2p_64b_address <= '1';
              end if;
              -- prepare a packet, first the header
              p2l_dma_current_state <= P2L_SETUP;
            end if;

          when P2L_SETUP =>
            -- if DMA length is bigger than the max PCIe payload size,
            -- we have to generate several read request
            if l2p_len_cnt > c_MAX_READ_REQ_SIZE then
              -- when max payload length is 1024, the header length field = 0
              l2p_len_header  <= c_MAX_READ_REQ_SIZE;
            else
              l2p_len_header  <= l2p_len_cnt(10 downto 0);
            end if;
            -- request access to PCIe bus
            pdm_arb_req_o         <= '1';
            p2l_dma_current_state <= P2L_HEADER;

          when P2L_HEADER =>
            if arb_pdm_gnt_i = '1' then
              -- clear access request to the arbiter
              -- access is granted until dframe is cleared
              pdm_arb_req_o    <= '0';
              -- send header
              pdm_arb_data(31 downto 29) <= "000";  -->  Traffic Class
              pdm_arb_data(28)           <= '0';    -->  Snoop
              pdm_arb_data(27 downto 24) <= "000" & l2p_64b_address; -->  Packet type = read req (32 or 64)
              if l2p_len_header = 1 then
                -- Last Byte Enable must be "0000" when length = 1
                pdm_arb_data(23 downto 20) <= "0000"; -->  LBE (Last Byte Enable)
              else
                pdm_arb_data(23 downto 20) <= "1111"; -->  LBE (Last Byte Enable)
              end if;
              pdm_arb_data(19 downto 16) <= "1111"; -->  FBE (First Byte Enable)
              pdm_arb_data(15 downto 13) <= "111";  -->  Reserved
              pdm_arb_data(12)           <= '0';    -->  VC (Virtual Channel)
              pdm_arb_data(11 downto 10) <= "01";   -->  CID
              pdm_arb_data(9  downto 0)  <= std_logic_vector(l2p_len_header (9 downto 0));  -->  Length (in words)
              pdm_arb_valid_o  <= '1';
              pdm_arb_dframe_o <= '1';
              if l2p_64b_address = '1' then
                -- if host address is 64-bit, we have to send an additionnal
                -- 32-word containing highest bits of the host address
                p2l_dma_current_state <= P2L_ADDR_H;
              else
                -- for 32-bit host address, we only have to send lowest bits
                p2l_dma_current_state <= P2L_ADDR_L;
              end if;
            end if;

          when P2L_ADDR_H =>
            -- send host address 32 highest bits
            pdm_arb_data          <= l2p_address_h;
            p2l_dma_current_state <= P2L_ADDR_L;

          when P2L_ADDR_L =>
            -- send host address 32 lowest bits
            pdm_arb_data          <= l2p_address_l;
            -- clear dframe signal to indicate the end of packet
            pdm_arb_dframe_o      <= '0';
            -- Subtract the number of word requested to generate a new read request if needed
            l2p_len_cnt <= l2p_len_cnt - l2p_len_header;
            l2p_address_l <= std_logic_vector(unsigned(l2p_address_l) + 4*l2p_len_header);
            p2l_dma_current_state <= P2L_WAIT_READ_COMPLETION;
            
          when P2L_WAIT_READ_COMPLETION =>
            -- End of the read request packet
            pdm_arb_valid_o <= '0';
            
            if pd_pdm_data_valid_i = '1' and pd_pdm_master_cpld_i = '1' then
              --  Received a word.
              l2p_len_header <= l2p_len_header - 1;
            end if;

            if dma_ctrl_abort_i = '1' then
              rx_error_t            <= '1';
              p2l_dma_current_state <= P2L_IDLE;
            elsif pd_pdm_master_cpld_i = '1' and pd_pdm_data_last_i = '1'
                  and l2p_len_header = 1
            then
              -- Note: a read request may result in multiple read completion.
              -- last word of read completion has been received
              if l2p_len_cnt /= 0 then
                -- A new read request is needed, DMA size > max payload
                p2l_dma_current_state <= P2L_SETUP;
              else
                -- indicate end of DMA transfer
                if is_next_item = '1' then
                  next_item_valid_o <= '1';
                else
                  dma_ctrl_done_t <= '1';
                end if;
                p2l_dma_current_state <= P2L_IDLE;
              end if;
            elsif pd_pdm_master_cpln_i = '1' then
              -- should not return a read completion without data
              completion_error      <= '1';
              p2l_dma_current_state <= P2L_IDLE;
            end if;

          when others =>
            p2l_dma_current_state <= P2L_IDLE;
            pdm_arb_req_o         <= '0';
            pdm_arb_data          <= (others => '0');
            pdm_arb_valid_o       <= '0';
            pdm_arb_dframe_o      <= '0';
            dma_ctrl_done_t       <= '0';
            next_item_valid_o     <= '0';
            completion_error      <= '0';
            rx_error_t            <= '0';

        end case;
      end if;
    end if;
  end process p_read_req_fsm;

  pdm_arb_data_o <= pdm_arb_data;

  ------------------------------------------------------------------------------
  -- Pipeline control signals
  ------------------------------------------------------------------------------
  p_ctrl_pipe : process (clk_i)
  begin
    if rising_edge(clk_i) then
      rx_error_d      <= rx_error_t;
      rx_error_o      <= rx_error_d;
      dma_ctrl_done_o <= dma_ctrl_done_t;
    end if;
  end process p_ctrl_pipe;

  ------------------------------------------------------------------------------
  -- Received data counter
  ------------------------------------------------------------------------------
  p_recv_data_cnt : process (clk_i)
  begin
    if rising_edge(clk_i) then
      if p2l_dma_current_state = P2L_ADDR_L then
          -- Store number of 32-bit data words to be received for the current read request
        p2l_data_cnt <= l2p_len_header;
      elsif p2l_dma_current_state = P2L_WAIT_READ_COMPLETION
            and pd_pdm_data_valid_i = '1'
            and pd_pdm_master_cpld_i = '1'
      then
          -- decrement number of data to be received
        p2l_data_cnt <= p2l_data_cnt - 1;
      end if;
    end if;
  end process p_recv_data_cnt;

  ------------------------------------------------------------------------------
  -- Next DMA item retrieve
  ------------------------------------------------------------------------------
  p_next_item : process (clk_i)
  begin
    if rising_edge(clk_i) then
      if p2l_dma_current_state = P2L_WAIT_READ_COMPLETION
         and is_next_item = '1' and pd_pdm_data_valid_i = '1'
      then
        -- next item data are supposed to be received in the right order !!
        case p2l_data_cnt(2 downto 0) is
          when "111" =>
            next_item_carrier_addr <= pd_pdm_data_i;
          when "110" =>
            next_item_host_addr_l <= pd_pdm_data_i;
          when "101" =>
            next_item_host_addr_h <= pd_pdm_data_i;
          when "100" =>
            next_item_len <= pd_pdm_data_i;
          when "011" =>
            next_item_next_l <= pd_pdm_data_i;
          when "010" =>
            next_item_next_h <= pd_pdm_data_i;
          when "001" =>
            next_item_attrib <= pd_pdm_data_i;
          when others =>
            null;
        end case;
      end if;
    end if;
  end process p_next_item;

  next_item_carrier_addr_o <= next_item_carrier_addr;
  next_item_host_addr_h_o  <= next_item_host_addr_h;
  next_item_host_addr_l_o  <= next_item_host_addr_l;
  next_item_len_o          <= next_item_len;
  next_item_next_l_o       <= next_item_next_l;
  next_item_next_h_o       <= next_item_next_h;
  next_item_attrib_o       <= next_item_attrib;

  ------------------------------------------------------------------------------
  -- Target address counter
  ------------------------------------------------------------------------------
  p_addr_cnt : process (clk_i)
  begin
    if rising_edge(clk_i) then
      if rst_n_i = '0' then
        dma_busy_error       <= '0';
        to_wb_fifo_wr        <= '0';
        to_wb_fifo_wr_d      <= '0';
      else
        to_wb_fifo_din_d <= to_wb_fifo_din;
        to_wb_fifo_wr_d  <= to_wb_fifo_wr;

        if dma_ctrl_start_p2l_i = '1' then
          if p2l_dma_current_state = P2L_IDLE then
            -- dma_ctrl_target_addr_i is a byte address and target_addr_cnt is a
            -- 32-bit word address
            target_addr_cnt      <= unsigned(dma_ctrl_carrier_addr_i(31 downto 2));
            -- stores byte swap info for the current DMA transfer
            to_wb_fifo_byte_swap <= dma_ctrl_byte_swap_i;
          else
            dma_busy_error <= '1';
          end if;
        elsif p2l_dma_current_state = P2L_WAIT_READ_COMPLETION
              and is_next_item = '0' and pd_pdm_data_valid_i = '1'
        then
          -- write target address and data to the sync fifo
          to_wb_fifo_wr                <= '1';
          to_wb_fifo_din(31 downto 0)  <= f_byte_swap(g_BYTE_SWAP, pd_pdm_data_i, to_wb_fifo_byte_swap);
          to_wb_fifo_din(61 downto 32) <= std_logic_vector(target_addr_cnt);
          -- increment target address counter
          target_addr_cnt              <= target_addr_cnt + 1;
        else
          dma_busy_error <= '0';
          to_wb_fifo_wr  <= '0';
        end if;
      end if;
    end if;
  end process p_addr_cnt;

  ------------------------------------------------------------------------------
  -- FIFO for transition between GN4124 core and wishbone clock domain
  ------------------------------------------------------------------------------
  ------------------------------------------------------------------------------
  -- Active low reset for fifos
  ------------------------------------------------------------------------------
  fifo_rst_n <= rst_n_i;

  -- Local resynced copy of fifo_rst_n to make sure that both sides of the fifo
  -- are reset if rst_n_i = '0'
  cmp_wb_fifo_rst_sync : gc_sync
    port map (
      clk_i     => wb_dma_clk_i,
      rst_n_a_i => wb_dma_rst_n_i,
      d_i       => fifo_rst_n,
      q_o       => wb_fifo_rst_n);

  -- Pipeline to_wb_fifo_full to help with timing. This requires the
  -- equivalent setting in g_ALMOST_FULL_THRESHOLD to prevent overflows.
  p_fifo_full_delay_reg : process (wb_dma_clk_i) is
  begin
    if rising_edge(wb_dma_clk_i) then
      -- we want proper registers to help with timing and
      -- having a reset prevents inferring of shift register.
      if wb_fifo_rst_n = '0' then
        to_wb_fifo_full_d <= (others => '0');
      else
        to_wb_fifo_full_d <= to_wb_fifo_full_d(to_wb_fifo_full_d'high-1 downto 0) & to_wb_fifo_full_next;
      end if;
    end if;
  end process p_fifo_full_delay_reg;

  to_wb_fifo_full <= to_wb_fifo_full_d(to_wb_fifo_full_d'high);

  cmp_to_wb_fifo : generic_async_fifo_dual_rst
    generic map (
      g_DATA_WIDTH            => 64,
      g_SIZE                  => g_FIFO_SIZE,
      g_SHOW_AHEAD            => TRUE,
      g_WITH_WR_FULL          => FALSE,
      g_WITH_WR_ALMOST_FULL   => TRUE,
      -- 20 less to give time to the GN4124 to react to P2L_RDY going low.
      g_ALMOST_FULL_THRESHOLD => g_FIFO_SIZE - c_SYNC_FIFO_FULL_DELAY - 20)
    port map (
      -- write port
      rst_wr_n_i       => fifo_rst_n,
      clk_wr_i         => clk_i,
      d_i              => to_wb_fifo_din_d,
      we_i             => to_wb_fifo_wr_d,
      wr_almost_full_o => to_wb_fifo_full_next,
      -- read port
      rst_rd_n_i       => wb_fifo_rst_n,
      clk_rd_i         => wb_dma_clk_i,
      q_o              => to_wb_fifo_dout,
      rd_i             => to_wb_fifo_rd,
      rd_empty_o       => to_wb_fifo_empty);

  -- pause transfer from GN4124 if fifo is (almost) full
  p2l_rdy_o <= not(to_wb_fifo_full);

  ------------------------------------------------------------------------------
  -- Wishbone master (write only)
  ------------------------------------------------------------------------------

  -- write only
  wb_dma_o.we <= '1';
  wb_dma_o.sel <= (others => '1');
  wb_dma_o.stb <= wb_dma_out_stb;

  --  Set when a wishbone transfer occurred.
  wb_dma_tfr <= wb_dma_out_stb = '1' and wb_dma_i.stall = '0';

  --  Read from fifo.
  --  Only when the fifo is not empty
  --  and either no previous data or previous data read.
  --  wb_dma_out_stb = '1' when there are previous data.
  to_wb_fifo_rd <= '1' when to_wb_fifo_empty = '0' and (wb_dma_tfr or wb_dma_out_stb = '0') else '0';

  -- Wishbone master process
  p_wb_master : process (wb_dma_clk_i)
  begin
    if rising_edge(wb_dma_clk_i) then
      if wb_fifo_rst_n = '0' then
        wb_dma_o.cyc <= '0';
        wb_dma_out_stb <= '0';
        wb_ack_cnt <= (others => '0');
      else
        if to_wb_fifo_rd = '1' then
          --  Data available, read them from the fifo.
          wb_dma_o.adr(31 downto 30) <= "00";
          wb_dma_o.adr(29 downto 0)  <= to_wb_fifo_dout(61 downto 32);
          wb_dma_o.dat               <= to_wb_fifo_dout(31 downto 0);

          -- Data/addresses are valid when fifo was just read.
          wb_dma_out_stb <= '1';
          wb_dma_o.cyc <= '1';
        else
          --  No read.
          if wb_dma_out_stb = '1' and wb_dma_i.stall = '1' then
            --  Data were not read, just wait.
            null;
          elsif to_wb_fifo_empty = '1' then
            --  No more data to produce.
            wb_dma_out_stb <= '0';

            if wb_ack_cnt = 0 then
              --  End of the burst
              wb_dma_o.cyc <= '0';
            end if;
          end if;
        end if;

        --  Track number of expected ack.
        if wb_dma_tfr and wb_dma_i.ack = '0' then
          wb_ack_cnt <= wb_ack_cnt + 1;
        elsif not wb_dma_tfr and wb_dma_i.ack = '1' then
          wb_ack_cnt <= wb_ack_cnt - 1;
        end if;
      end if;
    end if;
  end process p_wb_master;
end architecture arch;
