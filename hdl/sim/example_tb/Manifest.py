sim_tool   = "modelsim"
top_module = "main"
action     = "simulation"
target     = "xilinx"
syn_device = "xc6slx45t"
vcom_opt   = "-93 -mixedsvvh"

# Allow the user to override fetchto using:
#  hdlmake -p "fetchto='xxx'"
if locals().get('fetchto', None) is None:
  fetchto    = "../../ip_cores"

include_dirs = [
    "../gn4124_bfm",
    fetchto + "/general-cores/sim",
]

files = [
    "main.sv",
]

modules = {
    "local" :  [
        "../../../",
    ],
    "git" : [
        "https://ohwr.org/project/general-cores.git",
    ],
}
