onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/wb_dma_clk_i
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/wb_dma_current_state
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/p_wb_fsm/wb_dma_addr
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/p_wb_fsm/wb_dma_cnt_stb
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/p_wb_fsm/wb_dma_cnt_ack
add wave -noupdate -expand -group L2P -expand /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/wb_dma_o
add wave -noupdate -expand -group L2P -expand /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/wb_dma_i
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/data_fifo_wr
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/data_fifo_full
add wave -noupdate -expand -group L2P /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/data_fifo_din
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/clk_i
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/data_fifo_empty
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/data_fifo_rd
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/data_fifo_dout
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/dma_packet_len
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/l2p_dma_current_state
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/ldm_arb_valid_o
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/ldm_arb_dframe_o
add wave -noupdate -expand -group L2P -color Magenta /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_l2p_dma_master/ldm_arb_data_o
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/clk_i
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/p2l_dma_current_state
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/target_addr_cnt
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/pd_pdm_data_valid_i
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/pd_pdm_data_i
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/to_wb_fifo_wr_d
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/to_wb_fifo_din_d
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/to_wb_fifo_full
add wave -noupdate -expand -group P2L -color {Indian Red} /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/p2l_rdy_o
add wave -noupdate -expand -group P2L /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/wb_dma_clk_i
add wave -noupdate -expand -group P2L /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/to_wb_fifo_empty
add wave -noupdate -expand -group P2L /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/to_wb_fifo_rd
add wave -noupdate -expand -group P2L /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/to_wb_fifo_dout
add wave -noupdate -expand -group P2L /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/wb_dma_o
add wave -noupdate -expand -group P2L /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_p2l_dma_master/wb_dma_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_clk_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_cyc_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_stb_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_we_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_ack_o
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_adr_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_dat_i
add wave -noupdate -expand -group {DMA Controller} -color Thistle /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/wb_dat_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_start_l2p_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_start_p2l_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_start_next_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_abort_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_error_i
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_done_i
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_irq_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_direction_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_carrier_addr_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_host_addr_h_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_host_addr_l_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_len_o
add wave -noupdate -expand -group {DMA Controller} -color Cyan /main/DUT/cmp_wrapped_gn4124/gen_with_dma/cmp_dma_controller/dma_ctrl_byte_swap_o
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {226705189 ps} 0} {{Cursor 2} {129899342 ps} 1}
quietly wave cursor active 1
configure wave -namecolwidth 277
configure wave -valuecolwidth 210
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {611801400 ps}
